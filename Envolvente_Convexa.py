# Universidad Central del Ecuador
# Facultad de Ingeniería y Ciencias Aplicadas
# Computación Gráfica
# 04 de Enero del 2021
# Nombre: Sandra Ipiales
# Envolvente_Convexa
# Descripción: Visualización de envolvente Convexa con el algoritmo de envolvimiento de Regalo

import random as rand
import numpy as np
import matplotlib.pyplot as plt


num= 80 #Numero de puntos que se van a graficar
coordenadas=[]  #Vector para Almacenar coordenadas x,y

#Giro hacia  la derecha
def giro_derecha():
    array = [coordenadas[0], coordenadas[1]] #array con cordenada x,y
    for i in range(2, len(coordenadas)):
        array.append(coordenadas[i])
        while len(array) > 2 and np.linalg.det([array[-3], array[-2], array[-1]]) > 0:
            array.pop(-2)
    return array


def convex_hull():
    coordenadas.sort()
    long_superior = giro_derecha()
    coordenadas.reverse()
    long_bajo = giro_derecha()
    l = long_superior + long_bajo
    return l

#funcion Para graficar puntos, y poligono
def graficar(convex_pol, coord_points):
    # Acomodando listas adecuadas para graficar en matplot
    x_points = [i[0] for i in coord_points]
    y_points = [i[1] for i in coord_points]
    x_polygon = [i[0] for i in convex_pol]
    y_polygon = [i[1] for i in convex_pol]
    # Definiendo límites extremos de la gráfica
    x_lim_der = max(x_points) + 5
    y_lim_sup = max(y_points) + 5
    x_lim_izq = min(x_points) - 5
    y_lim_inf = min(y_points) - 5
    # Asignación de los límites extremos
    plt.xlim(x_lim_izq, x_lim_der)
    plt.ylim(y_lim_inf, y_lim_sup)
    # Graficación
    plt.title('Problema: Convex Hull')
    plt.xlabel('Eje de las abscisas')
    plt.ylabel('Eje de las ordenadas')
    plt.plot(x_points, y_points, 'ko')
    plt.plot(x_polygon, y_polygon, 'r-', linewidth = 2.0)
    plt.show()


#Almacenamos numeros aleatorios
for i in range(num):
    coordenadas.append([rand.uniform(0,80), rand.uniform(0,100), 1.0])
graficar(convex_hull(),coordenadas)